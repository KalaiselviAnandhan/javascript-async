function fetchRandomNumbers(){
    return new Promise((resolve,reject)=>{
        console.log('Fetching number...');
        setTimeout(() => {
            let randomNum = Math.floor(Math.random() * (100 - 0 + 1)) + 0;
            console.log('Received random number:', randomNum);
            resolve(randomNum);
        }, (Math.floor(Math.random() * (5)) + 1) * 1000);
    })
}

function fetchRandomString(){
    return new Promise((resolve,reject)=>{
        console.log('Fetching string...');
        setTimeout(() => {
            let result           = '';
            let characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
            let charactersLength = characters.length;
            for ( let i = 0; i < 5; i++ ) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
            }
            console.log('Received random string:', result);
            resolve(result);
        }, (Math.floor(Math.random() * (5)) + 1) * 1000);
    })
}

// fetchRandomNumbers((randomNum) => console.log(randomNum))
// fetchRandomString((randomStr) => console.log(randomStr))

// Task 1 Refactorization of promisified Functions into Async function
async function randomNumbersandString(){
    try{
      let randomnumberPromise=fetchRandomNumbers();
      let randomstringPromise=fetchRandomString();
      let randomnumber=await randomnumberPromise;
      let randomstring=await randomstringPromise;
      console.log(randomnumber,randomstring);
    }
    catch(err){
      console.log(err);
    }
}
randomNumbersandString();

// Task 2 Fetch a random number -> add it to a sum variable and print sum-> 
// fetch another random variable-> add it to the same sum variable and print the sum variable.
async function sumOfRandomNumbers(){
    let sum=0;
    try{
      let randomnumber1 =await fetchRandomNumbers()
      sum+=randomnumber1
      console.log("sum",sum);
      let randomnumber2 =await fetchRandomNumbers()
      sum+=randomnumber2;
      console.log("final sum",sum);
    }
    catch(error){
      console.log(error);
    }
}
sumOfRandomNumbers()

// Task 3 Fetch a random number and a random string simultaneously, concatenate their and print the concatenated string
async function stringConcatenation(){
    try{
      const [randomnumber,randomstring]= await Promise.all([fetchRandomNumbers(),fetchRandomString()])
      console.log(randomnumber+randomstring);
    }
    catch(error){
      console.log(error)
    }
  }
  stringConcatenation();

// Task 4 Fetch 10 random numbers simultaneously -> and print their sum.
async function randomNumbersSum(){
    try{
      const values=await Promise.all(Array(10).fill(0).map((element)=>fetchRandomNumbers()))
      console.log(values.reduce((acc,curr)=>acc+curr))
    }
    catch(error){
      console.log(error)
    }
}
randomNumbersSum();